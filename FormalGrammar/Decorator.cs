﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FormalGrammar
{
    internal static class Decorator
    {
        private static readonly Dictionary<string, Func<string, string>> _symbols = new Dictionary<string, Func<string, string>>
        {
            {"^", (t) => new CultureInfo("en-US", false).TextInfo.ToTitleCase(t.Replace("^", string.Empty))},
            {"$", (t) => t.Replace('$', '.')}
        };

        public static string DecorateText(string input)
        {
            return _symbols.Where(symbol => input.Contains(symbol.Key))
                .Aggregate(input, (current, symbol) => symbol.Value(current));
        }

        public static string StripText(string input)
        {
            return _symbols.Where(symbol => input.Contains(symbol.Key))
                .Aggregate(input, (current, symbol) => current.Replace(symbol.Key, string.Empty));
        }
    }
}