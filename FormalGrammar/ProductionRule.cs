﻿namespace FormalGrammar
{
    internal class ProductionRule : IWeighted
    {
        private readonly string _output;

        private readonly string _symbol;

        private readonly double _weight;

        public ProductionRule(string symbol, string output, double weight)
        {
            _symbol = symbol;
            _output = output;
            _weight = weight;
        }

        public string Output
        {
            get { return _output; }
        }

        public string Symbol
        {
            get { return _symbol; }
        }

        public double Weight
        {
            get { return _weight; }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != GetType())
                return false;

            return Equals((ProductionRule) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (_output != null ? _output.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_symbol != null ? _symbol.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ _weight.GetHashCode();
                return hashCode;
            }
        }

        protected bool Equals(ProductionRule other)
        {
            return string.Equals(_output, other._output) &&
                   string.Equals(_symbol, other._symbol) &&
                   _weight.Equals(other._weight);
        }
    }
}