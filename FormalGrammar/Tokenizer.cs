﻿using System.Collections.Generic;
using System.Linq;

namespace FormalGrammar
{
    internal class Tokenizer
    {
        private readonly IDictionary<string, List<ProductionRule>> _productionRules;

        public Tokenizer(IEnumerable<ProductionRule> productionRules)
        {
            _productionRules = productionRules.GroupBy(r => r.Symbol).ToDictionary(g => g.Key, g => g.ToList());
        }

        public IList<string> Tokenize(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            var tokens = new List<string>();
            var symbols = input.Split(' ');

            foreach (var symbol in symbols)
            {
                var stripped = Decorator.StripText(symbol);

                if (!_productionRules.ContainsKey(stripped))
                    tokens.Add(Decorator.DecorateText(symbol));
                else
                {
                    var processor = new ProductionRuleProcessor(_productionRules[stripped]);
                    tokens.AddRange(Tokenize(processor.Process(symbol)));
                }
            }

            return tokens;
        }
    }
}