﻿using System;

namespace FormalGrammar
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Press 'r' to regenerate paragraph, or anything else to exit.");
            Console.WriteLine();

            var rules = ProductionRulesFactory.GetProductionRules();

            var generator = new Generator("P", rules);
            var result = generator.Generate();

            Console.WriteLine(result);

            while (true)
            {
                if (Console.ReadKey().KeyChar != 'r')
                    break;

                Console.WriteLine();
                Console.WriteLine(generator.Generate());
            }
        }
    }
}