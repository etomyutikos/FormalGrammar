﻿namespace FormalGrammar
{
    internal interface IWeighted
    {
        double Weight { get; }
    }
}