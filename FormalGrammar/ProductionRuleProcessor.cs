﻿using System.Collections.Generic;

namespace FormalGrammar
{
    // TODO: Should probably build a cache of Processors lazily, and reuse.
    internal class ProductionRuleProcessor
    {
        private readonly IList<ProductionRule> _rules;

        public ProductionRuleProcessor(IList<ProductionRule> rules)
        {
            _rules = rules;
        }

        public string Process(string input)
        {
            var rule = WeightedSelector.Select(_rules);
            return input.Replace(rule.Symbol, rule.Output);
        }
    }
}