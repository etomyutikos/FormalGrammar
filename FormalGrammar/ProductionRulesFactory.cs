﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace FormalGrammar
{
    internal static class ProductionRulesFactory
    {
        public static IList<ProductionRule> GetProductionRules()
        {
            var jsons = Configuration.GetAllJson("*Rules");
            return jsons.SelectMany(j => ParseList(j.Value)).ToList();
        }

        public static ProductionRule Parse(string json)
        {
            return JsonConvert.DeserializeObject<ProductionRule>(json);
        }

        public static IList<ProductionRule> ParseList(string json)
        {
            return JsonConvert.DeserializeObject<List<ProductionRule>>(json);
        }
    }
}