﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FormalGrammar
{
    internal static class WeightedSelector
    {
        private static readonly Random _random = new Random();

        public static T Select<T>(IList<T> values) where T : IWeighted
        {
            var sum = values.Sum(v => v.Weight);
            var selector = _random.NextDouble() * sum;

            return Select(selector, values);
        }

        public static T Select<T>(double selector, IList<T> values) where T : IWeighted
        {
            var cumulative = 0.0;
            foreach (var value in values)
            {
                cumulative += value.Weight;

                if (selector <= cumulative)
                    return value;
            }

            return values.Last();
        }
    }
}