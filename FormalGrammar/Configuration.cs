﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FormalGrammar
{
    internal static class Configuration
    {
        private const string _DIRECTORY_PATH = @"Configurations/";

        public static IDictionary<string, string> GetAllJson(string pattern)
        {
            var p = pattern;
            if (!p.EndsWith(".json"))
                p += ".json";

            var directory = new DirectoryInfo(_DIRECTORY_PATH);
            var files = directory.GetFiles(p);

            if (!files.Any())
                return null;

            return files.ToDictionary(file => file.Name, file => File.ReadAllText(file.FullName));
        }

        public static string GetJson(string configuration)
        {
            var directory = new DirectoryInfo(_DIRECTORY_PATH);
            var files = directory.GetFiles("*.json");

            var file = files.SingleOrDefault(f => f.Name.StartsWith(configuration));
            return file == null ? null : File.ReadAllText(file.FullName);
        }
    }
}