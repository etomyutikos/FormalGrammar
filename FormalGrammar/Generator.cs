﻿using System.Collections.Generic;
using System.Linq;

namespace FormalGrammar
{
    internal class Generator
    {
        private readonly string _startSymbol;
        private readonly Tokenizer _tokenizer;

        public string StartSymbol
        {
            get { return _startSymbol; }
        }

        public Generator(string startSymbol, IEnumerable<ProductionRule> rules)
        {
            _startSymbol = startSymbol;

            _tokenizer = new Tokenizer(rules);
        }

        public string Generate()
        {
            var tokens = _tokenizer.Tokenize(_startSymbol);

            return tokens.Aggregate(string.Empty, (current, text) =>
            {
                if (string.IsNullOrEmpty(current))
                    return text;

                return current + " " + text;
            });
        }
    }
}