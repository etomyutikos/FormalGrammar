﻿Feature: Generator	

Scenario: Generate text using Generator
	Given a Generator instance with start symbol "P" and rules:
		| Symbol | Output | Weight |
		| P      | S S S  | 1.0    |
	When you call Generate
	Then you should get back a non-empty string

	Then the output should not begin with a " "
	
Scenario: Generate text respecting decorator symbols
	Given a Generator instance with start symbol "P" and rules:
		| Symbol | Output | Weight |
		| P      | S$     | 1.0    |
		| S      | A      | 1.0    |
	When you call Generate
	Then the output should be "A."

	Given a Generator instance with start symbol "P" and rules:
		| Symbol | Output | Weight |
		| P      | S$     | 1.0    |
		| S      | A      | 1.0    |
		| A      | alpha  | 1.0    |
	When you call Generate
	Then the output should be "alpha."