﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace FormalGrammar.Spec
{
    [Binding]
    public class WeightedSelectorSteps
    {
        private List<ProductionRule> _rules;
        private ProductionRule _output;

        [Given(@"a list of weighted ProductionRules")]
        public void GivenAListOfWeightedProductionRules(Table table)
        {
            _rules = table.ConvertToProductionRules();
        }

        [Then(@"you get the ProductionRule")]
        public void ThenYouGetTheProductionRule(Table table)
        {
            var rules = table.ConvertToProductionRules();
            var rule = rules.First();

            Assert.AreEqual(rule, _output);
        }

        [When(@"you select randomly")]
        public void WhenYouSelectRandomly()
        {
            _output = WeightedSelector.Select(_rules);
        }

        [When(@"you select with a value of (.*)")]
        public void WhenYouSelectWithAValueOf(Decimal p0)
        {
            _output = WeightedSelector.Select((double) p0, _rules);
        }
    }
}