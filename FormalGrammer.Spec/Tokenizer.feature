﻿Feature: Tokenizer

Scenario: Tokenize input symbol based on rules
	Given a new Tokenizer and the input symbol "P" with rules:
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
	When you tokenize the input "P"
	Then you get a list containing 2 output tokens matching
		| Symbol |
		| S      |
		| S      |

	Given a new Tokenizer and the input symbol "P" with rules:
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
		| S      | A      | 1.0    |
	When you tokenize the input "P"
	Then you get a list containing 2 output tokens matching
		| Symbol |
		| A      |
		| A      |

	Given a new Tokenizer and the input symbol "P" with rules:
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
		| S      | A B    | 1.0    |
		| A      | alpha  | 1.0    |
		| B      | beta   | 1.0    |
	When you tokenize the input "P"
	Then you get a list containing 4 output tokens matching
		| Symbol |
		| alpha  |
		| beta   |
		| alpha  |
		| beta   |

	Given a new Tokenizer and the input symbol "P" with rules:
		| Symbol | Output | Weight |
		| P      | S      | 1.0    |
		| S      | A$     | 1.0    |
		| A      | alpha  | 1.0    |
	When you tokenize the input "P"
	Then you get a list containing 1 output tokens matching
		| Symbol |
		| alpha. |

	Then you do blah