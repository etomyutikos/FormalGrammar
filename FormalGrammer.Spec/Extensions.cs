﻿using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace FormalGrammar.Spec
{
    internal static class Extensions
    {
        public static List<ProductionRule> ConvertToProductionRules(this Table table)
        {
            return table.CreateInstance(
                () => table.Rows.Select(
                    row => new ProductionRule(row["Symbol"], row["Output"], double.Parse(row["Weight"]))).ToList());
        }
    }
}