﻿Feature: Production Rules Factory

Scenario: Produce ProductionRule from JSON
	Given a single JSON object: "{ Weight: 1.0, Symbol: "P", Output: "S S S" }"
	When you parse it
	Then you get a ProductionRule

	Given a list of JSON objects: "[{ Weight: 1.0, Symbol: "P", Output: "S S S" }, { Weight: 1.0, Symbol: "P", Output: "S" }]"
	When you parse the list
	Then you get a list of ProductionRules