﻿using System.Collections.Generic;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace FormalGrammar.Spec
{
    [Binding]
    public class ProductionRulesFactorySteps
    {
        #region Parse single ProductionRule
        private string _jsonToParse;
        private ProductionRule _productionRule;

        [Given(@"a single JSON object: ""(.*)""")]
        public void GivenSomeValidJson(string p0)
        {
            _jsonToParse = p0;
        }


        [Then(@"you get a ProductionRule")]
        public void ThenYouGetAProductionRule()
        {
            Assert.IsInstanceOf(typeof(ProductionRule), _productionRule);
            Assert.AreEqual("P", _productionRule.Symbol);
            Assert.AreEqual("S S S", _productionRule.Output);
            Assert.AreEqual(1.0f, _productionRule.Weight);
        }

        [When(@"you parse it")]
        public void WhenYouParseIt()
        {
            _productionRule = ProductionRulesFactory.Parse(_jsonToParse);
        }
        #endregion

        #region ParseDocument list of ProductionRules

        private string _jsonDocumentToParse;
        private IList<ProductionRule> _productionRules;

        [Given(@"a list of JSON objects: ""(.*)""")]
        public void GivenAValidJsonDocumentContaining(string p0)
        {
            _jsonDocumentToParse = p0;
        }

        [When(@"you parse the list")]
        public void WhenYouParseTheList()
        {
            _productionRules = ProductionRulesFactory.ParseList(_jsonDocumentToParse);
        }

        [Then(@"you get a list of ProductionRules")]
        public void ThenYouGetAListOfProductionRules()
        {
            Assert.AreEqual(2, _productionRules.Count);

            Assert.AreEqual("P", _productionRules[0].Symbol);
            Assert.AreEqual("S S S", _productionRules[0].Output);
            Assert.AreEqual(1.0f, _productionRules[0].Weight);

            Assert.AreEqual("P", _productionRules[1].Symbol);
            Assert.AreEqual("S", _productionRules[1].Output);
            Assert.AreEqual(1.0f, _productionRules[1].Weight);
        }

        #endregion
    }
}