﻿using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace FormalGrammar.Spec
{
    [Binding]
    public class ProductionRuleProcessorSteps
    {
        private ProductionRuleProcessor _processor;
        private string _output;

        [Given(@"a new processor with rules")]
        public void GivenANewProcessorWithRules(Table table)
        {
            var rules = table.ConvertToProductionRules();

            _processor = new ProductionRuleProcessor(rules);
        }

        [Then(@"you should get output ""(.*)""")]
        public void ThenYouShouldGetOutput(string p0)
        {
            Assert.AreEqual(p0, _output);
        }

        [When(@"you process input symbol ""(.*)""")]
        public void WhenYouProcessInputSymbol(string p0)
        {
            _output = _processor.Process(p0);
        }
    }
}