﻿Feature: Weighted Selector

Scenario: Select a value from a weighted list
	Given a list of weighted ProductionRules
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
		| P      | S      | 0.0    |
	When you select randomly
	Then you get the ProductionRule
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |

	Given a list of weighted ProductionRules
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
		| P      | S      | 1.0    |
	When you select with a value of 1.1
	Then you get the ProductionRule
		| Symbol | Output | Weight |
		| P      | S      | 1.0    |

	When you select with a value of 0.9
	Then you get the ProductionRule
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
