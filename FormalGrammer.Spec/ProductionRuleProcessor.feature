﻿Feature: Production Rule Processor

Scenario: Expand symbols into outputs
	Given a new processor with rules
		| Symbol | Output | Weight |
		| P      | S S    | 1.0    |
	When you process input symbol "P"
	Then you should get output "S S"