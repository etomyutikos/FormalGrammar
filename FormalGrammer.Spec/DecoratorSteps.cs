﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace FormalGrammar.Spec
{
    [Binding]
    public class DecoratorSteps
    {
        private string _input;
        private string _output;

        [Given(@"an output string ""(.*)""")]
        public void GivenAnOutputString(string p0)
        {
            _input = p0;
        }

        [Then(@"the string returned is ""(.*)""")]
        public void ThenTheStringReturnedIs(string p0)
        {
            Assert.AreEqual(p0, _output);
        }

        [When(@"it is decorated")]
        public void WhenItIsDecorated()
        {
            _output = Decorator.DecorateText(_input);
        }

        [When(@"it is stripped")]
        public void WhenItIsStripped()
        {
            _output = Decorator.StripText(_input);
        }
    }
}