﻿using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace FormalGrammar.Spec
{
    [Binding]
    public class FormalGrammarSteps
    {
        private Generator _generator;
        private string _output;

        [Given(@"a Generator instance with start symbol ""(.*)"" and rules:")]
        public void AGeneratorInstanceWithStartSymbolAndRules(string p0, Table table)
        {
            var rules = table.ConvertToProductionRules();

            _generator = new Generator(p0, rules);
        }

        [Then(@"you should get back a non-empty string")]
        public void ThenYouShouldGetBackANon_EmptyString()
        {
            Assert.IsFalse(string.IsNullOrEmpty(_output));
        }

        [When(@"you call Generate")]
        public void WhenYouCallGenerate()
        {
            _output = _generator.Generate();
        }

        [Then(@"the output should be ""(.*)""")]
        public void ThenTheOutputShouldEndWithA(string p0)
        {
            Assert.AreEqual(p0, _output);
        }

        [Then(@"the output should not begin with a ""(.*)""")]
        public void ThenTheOutputShouldNotBeginWithA(string p0)
        {
            Assert.IsFalse(_output.StartsWith(p0), string.Format("Output should not begin with a '{0}'.", p0));
        }

    }
}