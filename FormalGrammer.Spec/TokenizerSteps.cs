﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace FormalGrammar.Spec
{
    [Binding]
    public class TokenizerSteps
    {
        private Tokenizer _tokenizer;
        private IList<string> _output;

        [Given(@"a new Tokenizer and the input symbol ""(\w+)"" with rules:")]
        public void GivenANewTokenizerAndTheInputSymbolWithRules(string p0, Table table)
        {
            var rules = table.ConvertToProductionRules();

            _tokenizer = new Tokenizer(rules);
        }

        [Then(@"you get a list containing (.*) output tokens matching")]
        public void ThenYouGetAListContainingOutputTokensMatching(int p0, Table table)
        {
            Assert.AreEqual(p0, _output.Count);
            for (var i = 0; i < p0; i++)
                Assert.AreEqual(table.Rows[i]["Symbol"], _output[i]);
        }


        [When(@"you tokenize the input ""(\w+)""")]
        public void WhenYouTokenizeTheInput(string p0)
        {
            _output = _tokenizer.Tokenize(p0);
        }
    }
}