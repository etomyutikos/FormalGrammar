﻿Feature: Decorator

Scenario: Handle caret decorator symbol in text
	Given an output string "^bob"
	When it is stripped
	Then the string returned is "bob"

	When it is decorated
	Then the string returned is "Bob"

Scenario: Handle dollar sign decorator symbol in text
	Given an output string "bob$"
	When it is stripped
	Then the string returned is "bob"

	When it is decorated
	Then the string returned is "bob."

Scenario: Handle combinations of decorator symbols
	Given an output string "^bob$"
	When it is stripped
	Then the string returned is "bob"

	When it is decorated
	Then the string returned is "Bob."